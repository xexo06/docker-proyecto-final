import { Component, OnInit } from '@angular/core';

import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { ProductoService} from 'src/app/services/producto.service';


import Swal from 'sweetalert2';



@Component({
  selector: 'app-producto',
  templateUrl: './producto.component.html',
  styleUrls: ['./producto.component.sass']
})

export class ProductoComponent implements OnInit {

  form: FormGroup;
  imagePreview: string;



  constructor(private fb: FormBuilder, private _productoService: ProductoService) { 

    this.crearFormulario();
  }

  ngOnInit(): void {
  }



  get nombreNoValido() {
    return this.form.get('nombre').invalid && this.form.get('nombre').touched;
  }

  get proveedorNoValido() {
    return this.form.get('proveedor').invalid && this.form.get('proveedor').touched;
  }

 
  get fileNoValido() {
    return this.form.get('imagenBin').invalid && this.form.get('imagenBin').touched;
  }






  onFileChange(event) {
    let blob = new Blob(["\x01\x02\x03\x04"]);
    const reader = new FileReader();
 
    if(event.target.files && event.target.files.length) {
     
      const file = (event.target as HTMLInputElement).files[0];
      this.form.patchValue({imagenBin: file}); 
      this.form.get('imagenBin').updateValueAndValidity();
  
      reader.onload = () => {
        this.imagePreview = reader.result.toString();
        
       
      };
      reader.readAsDataURL(file);
   
    }
  }

crearFormulario() {
    
  this.form = this.fb.group(
    {
      nombre: ['', [Validators.required,]],
      proveedor: ['', [Validators.required,]],
      imagenBin: ['', [Validators.required,]],
    },
  );

}

    guardar(){
   
  
      if (this.form.invalid) {
        console.log('formulario invalido');
        Object.values(this.form.controls).forEach((control) => {
          if (control instanceof FormGroup) {
            Object.values(control.controls).forEach((control) => control.markAsTouched());
          } else {
            control.markAsTouched();
          }
        });
      } else {
        Swal.showLoading();
          this._productoService.guardarProducto(this.form).subscribe(
            (resp) => {
           
              Swal.close();
              Swal.fire({
                title: 'Proceso Finalizado ',
                text: 'precione ok',
              }).then((resultado) => {
                if (resultado.value) {
                  window.location.reload();
                }
              });
            },
  
            (error) => {
          
              Swal.fire({
                title: 'Error ',
                text: 'Vuelva a intentar',
              });
            }
          );
      }
    }



}
