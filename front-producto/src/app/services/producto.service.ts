import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ProductoModel} from 'src/app/models/producto.model';
import { map } from 'rxjs/operators';
import { FormGroup ,FormControl} from '@angular/forms';

// variable env
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})

export class ProductoService {

  url = environment.urlServicioProducto;

  constructor(private http: HttpClient) { 


  }

  obtenerProductos() {
  
    return this.http
      .get<ProductoModel[]>(`${this.url}/obtener`)
      .pipe(
        map(
          (resp) => {
            return resp['data'];
          },
          (error) => {
            return error;
          }
        )
      );
        }

  guardarProducto(form: FormGroup){
    
    const formData = new FormData();

    formData.append('nombre',  form.controls['nombre'].value);
    formData.append('proveedor',  form.controls['proveedor'].value);
 
    formData.append('file', form.controls['imagenBin'].value);

  


    return this.http.post<object>(`${this.url}/crear`,formData);

  }

  



}
