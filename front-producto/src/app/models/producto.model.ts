export class ProductoModel {
  _id: string;
  nombre: string;
  proveedor: string;
  imagenUrl: string;
  file: File;
}
