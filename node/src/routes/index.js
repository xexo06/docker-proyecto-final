const productoRoutes = require('./producto-routes');


module.exports = app => {

    app.use('/api/v1/producto', productoRoutes);

}