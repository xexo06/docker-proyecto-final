const express = require('express');

const productoController = require('../controllers/producto-controller');
const { uploader } = require('../middlewares/imgAdress');

const router = express.Router();

router.post('/crear', uploader.single('file'), productoController.crearProducto);
router.post('/crearSinImagen', productoController.crearSinImagenProducto);
router.put('/actualizar/:productoId', productoController.actualizarProducto);
router.get('/obtener', productoController.obtenerProducto);
router.post('/eliminar/:productoId', productoController.eliminarProducto);


module.exports = router;