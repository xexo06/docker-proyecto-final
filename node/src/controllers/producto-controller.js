const productoModel = require('../mongo/models/producto')

const crearProducto = async(request, response) => {

    try {
        console.log("request.body", request.body);
        const { nombre, proveedor, file } = request.body;
        const { imagenUrl } = request.imgUrl;




        // const { nombre, proveedor, imagen } = producto;
        // const {} = imgFile;


        const nuevoProducto = { nombre, proveedor, magePath: request.file.path }
        const productoPush = new productoModel(nuevoProducto);

        await productoModel.create({
            nombre,
            proveedor,
            imagenUrl,

        });
        response.send({ status: 'ok', message: 'producto creado' });
    } catch (error) {
        console.log(error);
        if (error.code && error.code === 11000) {
            response
                .status(400)
                .send({ status: 'DUPLICATED_VALUES', message: error.keyValue });
            return;
        }
        response.status(500).send({ status: 'error', message: error.message });
    }
};


const crearSinImagenProducto = async(request, response) => {

    try {

        const { nombre, proveedor, } = request.body;



        await productoModel.create({
            nombre,
            proveedor,
        });
        response.send({ status: 'ok', message: 'producto creado' });
    } catch (error) {
        console.log(error);
        if (error.code && error.code === 11000) {
            response
                .status(400)
                .send({ status: 'DUPLICATED_VALUES', message: error.keyValue });
            return;
        }
        response.status(500).send({ status: 'error', message: error.message });
    }
};


const obtenerProducto = async(request, response) => {

    try {
        const productos = await productoModel.find();
        response.status(200).send({ status: 'ok', data: productos });
    } catch (error) {
        console.log('error', error);
        response.status(500).send({ status: 'error', message: error.message });
    }
};

const actualizarProducto = async(request, response) => {

    const { nombre, proveedor, imagen } = request.body;
    productoModel.findByIdAndUpdate(request.params.productoId, {
            nombre: nombre,
            proveedor: proveedor,
            imagen: imagen

        }, { new: true })
        .then(nombre => {
            if (!nombre) {
                return response.status(404).send({
                    message: "Producto no encontrado " + request.params.productoId
                });
            }
            response.send(nombre);
        }).catch(err => {
            if (err.kind === 'ObjectId') {
                return response.status(404).send({
                    message: "Producto no encontrado " + request.params.productoId
                });
            }
            return response.status(500).send({
                message: "Error al actualizar al producto con id:" + request.params.productoId
            });
        });
};

const eliminarProducto = async(request, response) => {

    productoModel.findByIdAndRemove(request.params.productoId)
        .then(nombre => {
            if (!nombre) {
                return response.status(404).send({
                    message: "producto no encontrado " + request.params.productoId
                });
            }
            response.send({ message: "producto " + request.params.productoId + " producto eliminado!" });
        }).catch(err => {
            if (err.kind === 'ObjectId' || err.name === 'NotFound') {
                return response.status(404).send({
                    message: "producto no encontrado  " + request.params.productoId
                });
            }
            return response.status(500).send({
                message: "producto no encontradoproducto no encontrado " + request.params.productoId
            });
        });
};


module.exports = {
    crearProducto,
    crearSinImagenProducto,
    obtenerProducto,
    actualizarProducto,
    eliminarProducto,
};