const { v1: uuidv1 } = require('uuid');
const multer = require('multer');
const path = require('path');


const storage = multer.diskStorage({

    destination: function(req, file, cb) { //Indica la carpeta de destino
        console.log("entro a la wea");
        cb(null, path.join(__dirname, '../../../front-producto/src/assets/img/productos'));

    },
    filename: function(req, file, cb) { //Indica el nombre del archivo
        console.log("entro a la wea");
        numeroUnico = uuidv1();
        req.imgUrl = {
            imagenUrl: `assets/img/productos/${req.body.nombre}-${numeroUnico}-${file.originalname.split('.')[0]}.${file.mimetype.split('/')[1]}`
        }

        console.log("req id usuario", req.body);
        console.log("req id usuario", file);

        cb(null, `${req.body.nombre}-${numeroUnico}-${file.originalname.split('.')[0]}.${file.mimetype.split('/')[1]}`);

    }
});
const uploader = multer({ storage }); //Se guardara la imagen en el servidor

module.exports = { uploader };