const jwt = require('jsonwebtoken');

const isValidHostname = (request, response, next) => {
    const validacionHost = ['sergio.com', 'localhost'];
    if (validacionHost.includes(request.hostname)) {
        next();
    } else {
        response.status(403).send({ status: 'ACCESS_DENIED' });
    }
};


const isAuth = (request, response, next) => {
    try {
        console.log('req.headers', request.headers);
        const { token } = request.headers;
        if (token) {
            const data = jwt.verify(token, process.env.JWT_SECRET);
            console.log("data:", data);
            if (data.userId !== request.body.userId && data.role !== 'admin') {
                throw {
                    code: 403,
                    status: 'ACCESS_DENIED',
                    message: 'Missing permission or invalid role',
                };
            }
            request.sessionData = { userID: data.userId };
            next();
        } else {
            throw {
                code: 403,
                status: 'ACCESS_DENIED',
                message: 'Missing header token',
            };
        }
    } catch (error) {
        response.status(error.code || 500).send({ status: error.status || 'ERROR', message: error.message });
    }
};

module.exports = { isValidHostname, isAuth };