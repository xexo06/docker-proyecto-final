const mongoose = require('mongoose');


const { Schema } = mongoose;

const productoSchema = new Schema({
    nombre: {
        type: String,
        require: true,
    },
    proveedor: {
        type: String,
        require: true,

    },
    imagenUrl: {
        type: String,

    },
}, {
    timestamps: true,
});

const model = mongoose.model('Producto', productoSchema);

module.exports = model;