const express = require('express');
const bodyParser = require('body-parser');
const dotenv = require('dotenv');
const mongoose = require('mongoose');
const cors = require('cors');



dotenv.config();


const routes = require('./routes');


const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());


routes(app);


const { PORT, MONGO2 } = process.env;


mongoose.connect(MONGO2, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false,
        useCreateIndex: true,
    })
    .then(() => {
        console.log('connected to mongodb');

        app.listen(PORT, () => {
            console.log(`running on ${PORT}`);
        });
    })
    .catch((error) => {
        console.log('mongdb error', error);
    });